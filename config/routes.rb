Rails.application.routes.draw do
	get '/names', action: :index, controller: 'api/v1/names'
	post '/name', action: :create, controller: 'api/v1/names'
	get '/', action: :home, controller: 'api/v1/names'
	# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
