class Api::V1::NamesController < ApplicationController
	def index
		@names = Name.all
		render json: @names
	end
	def create
		@name = Name.new(name_params)
		@name.save
	end
	def home
  		render :json => 200
	end
	private
	def name_params
	  params.require(:name).permit(:name)
	end	
end
